#!/usr/bin/env python3
# Monopoly LED Assign
# Author: Keegan Cruickshank (keegan@cloudpixel.com.au)

from neopixel import *
import sys

# LED strip configuration:
LED_COUNT      = 28      # Number of LED pixels.
LED_PIN        = 18      # GPIO pin connected to the pixels (18 uses PWM!).
LED_FREQ_HZ    = 800000  # LED signal frequency in hertz (usually 800khz)
LED_DMA        = 10      # DMA channel to use for generating signal (try 10)
LED_BRIGHTNESS = 150     # Set to 0 for darkest and 255 for brightest
LED_INVERT     = False   # True to invert the signal (when using NPN transistor level shift)
LED_CHANNEL    = 0       # set to '1' for GPIOs 13, 19, 41, 45 or 53

def updateLights(data):
    """Updates the strip with colors from data"""
    for j in range(28):
        current = (j * 3)
        strip.setPixelColor(j, Color(data[current], data[current + 1], data[current + 2]))

# Main program logic follows:
if __name__ == '__main__':
    strip = Adafruit_NeoPixel(LED_COUNT, LED_PIN, LED_FREQ_HZ, LED_DMA, LED_INVERT, LED_BRIGHTNESS, LED_CHANNEL)
    strip.begin()
    data = sys.argv
    data.pop(0)
    updateLights(strip, data)
